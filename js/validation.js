// Wait for the DOM to be ready
$(function() {
  // Initialize form validation on the registration form.
  // It has the name attribute "registration"
  $("form[name='hallname']").validate({
    // Specify validation rules
    rules: {
      // The key name on the left side is the name attribute
      // of an input field. Validation rules are defined
      // on the right side
      hallname: "required",
	  price:"required",
	  rentprice:"required",
      categoryid: {required:true},
	  item:{required:true}
	  
    },
    // Specify validation error messages
    messages: {
      hallname: "Please enter your hallname",
	  
	  price:"Please enter your Price",
	  rentprice:"Please enter your Rentprice",
      categoryid: {
                required: 'Please select a category'
            },
			 item: {
                required: 'Please select a item'
            },
			
    },
	  errorElement: "div",
errorPlacement: function(error, element) {

            if (element.parent().hasClass('group')){
                element = element.parent();
            }


            offset = element.offset();
            error.insertBefore(element)
            error.addClass('message');  // add a class to the wrapper
            error.css('position', 'relative');
          
    },
    // Make sure the form is submitted to the destination defined
    // in the "action" attribute of the form when valid
    submitHandler: function(form) {
      form.submit();
    }
	
   });
   
   
   
   $("form[name='bookinghall']").validate({
    // Specify validation rules
    rules: {
      // The key name on the left side is the name attribute
      // of an input field. Validation rules are defined
      // on the right side
      price: "required",
	  bookdate:"required",
	  enddate:"required",
      cat: {required:true},
	  hallname:{required:true},
	  type:{required:true},
	   
    },
    // Specify validation error messages
    messages: {
      cate: "Please enter your cate",
	  
	  bookdate:"Please enter your startdate",
	  enddate:"Please enter your enddate",
      hallname: {required: 'Please select a hallname'},
	  cat: {required: 'Please select a cate'},	
	  type: {required: 'Please select a type'},
	  
    },
	  errorElement: "div",
errorPlacement: function(error, element) {

            if (element.parent().hasClass('group')){
                element = element.parent();
            }


            offset = element.offset();
            error.insertBefore(element)
            error.addClass('message');  // add a class to the wrapper
            error.css('position', 'relative');
          
    },
    // Make sure the form is submitted to the destination defined
    // in the "action" attribute of the form when valid
    submitHandler: function(form) {
      form.submit();
    }
	
   });
  
  //homepage
  
  
  
  
   
   
   
});