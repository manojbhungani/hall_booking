$(document).ready(function() {
    $("#Category").val("<?php echo $_POST['category'];?>");
    $("#HallName").val("<?php echo $_POST['hallname'];?>");
    $("#Year").val("<?php echo $_POST['year'];?>");
	$("#Month").val("<?php echo $_POST['month'];?>");
 
	$('#calendar').fullCalendar({
      header: {
        left: 'prev,next today',
        center: 'title',
        right: 'month'
		
		//right: 'month,agendaWeek,agendaDay,listWeek'
      },
      //defaultmonth: '2018-03-12',
	//var month = $('select#Month option:selected').val();
	//var year = $('select#Year option:selected').val();
	
	
	  defaultDate: moment('<?php echo $year,-$month?>'),
      navLinks: true, // can click day/week names to navigate views
      editable: true,
      eventLimit: true, // allow "more" link when too many events
      events: [
        {
          title: 'All Day Event',
          start: '2018-03-01',
        },
        {
          title: 'Long Event',
          start: '2018-09-10',
          end: '2018-09-10'
        },
        {
          id: 999,
          title: 'Repeating Event',
          start: '2018-03-09T16:00:00'
        },
        {
          id: 999,
          title: 'Repeating Event',
          start: '2018-03-16T16:00:00'
        },
        {
          title: 'Conference',
          start: '2018-03-11',
          end: '2018-03-13'
        },
        {
          title: 'Meeting',
          start: '2018-03-12T10:30:00',
          end: '2018-03-12T12:30:00'
        },
        {
          title: 'Lunch',
          start: '2018-03-12T12:00:00'
        },
        {
          title: 'Meeting',
          start: '2018-03-12T14:30:00'
        },
        {
          title: 'Happy Hour',
          start: '2018-03-12T17:30:00'
        },
        {
          title: 'Dinner',
          start: '2018-03-12T20:00:00'
        },
        {
          title: 'Birthday Party',
          start: '2018-03-13T07:00:00'
        },
        {
          title: 'Click for Google',
          url: 'http://google.com/',
          start: '2018-09-10',
          end: '2018-09-10'
        }
      ]
    });
});
