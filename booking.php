<?php 
include_once("Conection.php");
 $categoryErr ="";
 $category ="";
 $hallnameErr ="";
 $hallname ="";
 $dateErr ="";
 $enddateErr="";
 $hallname ="";
 $bookingtypeErr="";
 $bookingtype="";
 $priceErr="";
 $price="";
 $startdate="";
 $startdateErr="";
if(isset($_POST['submit']))
{
	if(empty($_POST["cat"]))
	{
		$categoryErr="required"; 
	}
	if(empty($_POST["hallname"]))
	{	
	    $hallnameErr="required";
	}
	if(empty($_POST["bookdate"]))
	{
	     $dateErr ="required";
	}
	if(empty($_POST["enddate"]))
	{
	     $enddateErr ="required";
	}
	if(empty($_POST["price"]))
	{
	     $priceErr ="required";
	}
	if(empty($_POST["type"])){
		$bookingtypeErr="required";
	}
	   else{
		   $mysqli=connection();
		    $category=$_POST['cat'];
			$hallname=$_POST['hallname'];
			$price=$_POST['price'];
			$bookingtype=$_POST['type'];
			$date = date_create_from_format('d/m/Y',$_POST['bookdate']);
			$date = date_format($date,'Y-m-d');
			$enddate = date_create_from_format('d/m/Y',$_POST['enddate']);
			$enddate = date_format($enddate,'Y-m-d');
			$exitcat=mysqli_query($mysqli,"select * from hall_booking where date='$date' and booking_type='$bookingtype' ");
		    $find = mysqli_num_rows($exitcat);
			if($find==1){
				echo "<script>alert('your select date already booking');</script>";
			}
			else{
				$ins=mysqli_query($mysqli,"insert into hall_booking (hall_category_id,hall_id,date,booking_type,price,enddate) values ('$category','$hallname','$date','$bookingtype','$price','$enddate')");
			if($ins)
			{
				echo "<script>alert('Your Booking is successfully Done');</script>";
			}
			else
			{
				echo mysqli_error();
			}
			}
			
			
	     }
    
}


?>
<html>
    <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	<title>Registration Form</title>

	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link href = "https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel = "stylesheet">
	<script src = "https://code.jquery.com/jquery-1.10.2.js"></script>
	<script src = "https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
	<script src="https://cdn.jsdelivr.net/jquery.validation/1.15.1/jquery.validate.min.js"></script>
	<script src="js/validation.js"></script>
	</head>
	
	<style>
     .error{
	 color:red;
     }
     label.error
     {
     color:red;
     font-family:verdana, Helvetica;
     }
    </style>
	
	<body>
	<div class="container">
	<a href='index.php' class="btn btn-info" align="center">home</a>
	<div class="panel panel-primary" style="margin:20px;">
	<div class="panel-heading"><h3 class="panel-title" align="center">Hall Availability Details</h3></div>
	<form action="" method="post" id="bookinghall" name="bookinghall">
	<div class="col-md-12 col-sm-12">
	<div class="form-group col-md-6 col-sm-6">
	<div class="form-group">
	<label for="category">Select category:</label>
	<select name='cat' id="Category"  class="form-control" size='1'>
    <option value=''>..Select...</option>
	<?php
	$mysqli=connection();
	$dd_res=mysqli_query($mysqli,"Select * from hall_category");
	while($r=mysqli_fetch_row($dd_res))
	{ 
	echo "<option value='$r[0]'> $r[1] </option>";
	}
	?>
	</select>
	</div>
	<div class="form-group">
	<label for="hallname">Select hallname:</label>
	<select name='hallname' id="hallname"  class="form-control" size='1'>
     <option value=''>..Select...</option>
	<?php
	if(isset($_POST['hallname']))
		{
			$id=$_POST['hallname'];
			//print_r($id);
			//exit();
			$dd_res=mysqli_query($mysqli,"Select * from hall_name where hall_id='$id'");
            while($r=mysqli_fetch_row($dd_res))
            { 
               echo "<option value='$r[0]'> $r[1] </option>";
            }
		}
	?>
	</select>
	</div>
	<label for="sel1">hall price/rent</label><br>
	<input type = "text" id = "price" name="price" value="" readonly>
	</div>
	
	<div class="form-group col-md-6 col-sm-6">
	<div class="form-group">
    <label for="type">Select type:</label>
	<select name='type'  class="form-control" size='1'>
    <option value=''>..Select...</option>
	<option value="1">Booked For One Shift</option>
	<option value="2">Booked For Two Shift</option>
	<option value="3">Booked For Full Day</option>
	</select>
	</div>
	<div class="form-group">
	<label for="bookdate">Select Booking From Date:</label><br>
	<input type = "text" autosuggest="off" id = "datepicker-13" name="bookdate" value="<?php echo $startdate;?>">
	</div>
	<div class="form-group">
	<label for="enddate">Select Booking To Date:</label><br>
	<input type = "text" autosuggest="off" id = "datepicker-12" name="enddate">
	</div>
	</div>
	
	</div>
	<div class="row"> 
	<div class="form-group col-md-4 col-sm-4">
	</div>
	<div class="form-group col-md-4 col-sm-4">
	<div class="form-group" align="center">
	<input type="submit" id="click" class="btn btn-primary" name="submit" value="submit" />
	<a href='index.php' class="btn btn-info" align="center">View-Hall-Availability</a>
	</div>
	</div>
	<div class="form-group col-md-4 col-sm-4">

	</div>
	</div>
	</div>	
	</form>
	</div>

	</div>  


	</body>
    <script>
         $(function() {
            $( "#datepicker-13" ).datepicker({dateFormat:'dd/mm/yy'});
           // $( "#datepicker-13" ).datepicker("show");
			
         });
		  $(function() {
            $( "#datepicker-12" ).datepicker({dateFormat:'dd/mm/yy'});
           // $( "#datepicker-13" ).datepicker("show");
			
         });
		 
		 $(document).ready(function(){
	$("#Category").on("change",function(){
		name = $(this).val();
		$.ajax({
		 type: "POST",
		 url: "function.php",
		  data: { 'func_name': "get_categoryid",'category_id': name }
	   }).done(function (data) {
		 $("#hallname").html(data);
	   });
	});
	
	$("#hallname").on("change",function(){
		name = $(this).val();
		$.ajax({
		 type: "POST",
		 url: "price.php",
		  data: { 'hall_name': "get_price",'hall_id': name }
	   }).done(function (data) {
		   //alert(data);
		 $("#price").val(data);
	   });
	});
	
	
});  

      </script>
 
</html>


